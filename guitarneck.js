function draw_fretboard() {
  // PARAMETERS
  const my_l = 1250;
  const my_w = 150;
  const dot_r = 25;
  const font_size = 12;
  const no_frets = 24;
  //const fret_l = my_l/(no_frets + 1.0);
  const fret_l = my_l/no_frets;
  const string_sep = my_w/6;
  const bubble_d = 0.80*string_sep;
  // COLOURS
  // https://www.w3schools.com/tags/ref_colornames.asp
  const fretboard_colour = 'BurlyWood';
  const fret_colour = 'DimGray';
  const string_colour = 'DarkGoldenRod';
  const dot_colour = 'Black'
  const bubble_opacity = 0.75

  // START DRAWING
  //var draw = SVG().addTo('body').size((my_l), (my_w+string_sep))
  var draw = SVG().addTo('body').size(my_l, my_w)
  draw.addClass('svgFretboard')

  // NECK
  var board = draw.rect(my_l, my_w);
  board.fill(fretboard_colour);

  // FRETS
  for ( var x=0; x<=no_frets; x++){
    var fret = draw.line( x*fret_l, 0, x*fret_l, my_w );
    fret.stroke({ width:4 });
    // NUT IS BIGGER
    if (x == 0) {
      fret.stroke({ width:8 });
    }
    fret.stroke(fret_colour);
  }

  // STRINGS
  for (y=0; y<6; y++){
    var y_value = 0.5*string_sep + y*string_sep;
    var string = draw.line( 0, y_value, my_l, y_value );
    string.stroke({ width:(0.6*y+2) });
    string.stroke(string_colour);
  }

  // DECORATIONS
  const dot_x = [3,5,7,9,15,17,19,21];
  for (x of dot_x) {
    var dot = draw.circle(dot_r);
    dot.fill( dot_colour )
    dot.center( (x*fret_l - 1.5*fret_l + fret_l), (0.5*my_w) );
  }
  x = 12;
  var dot = draw.circle(dot_r);
  dot.fill( dot_colour )
  dot.center((fret_l*x-1.5*fret_l+fret_l),(1.0*string_sep));
  var dot = draw.circle(dot_r);
  dot.fill( dot_colour )
  dot.center((fret_l*x-1.5*fret_l+fret_l),(5.0*string_sep));
  x = 24;
  var dot = draw.circle(dot_r);
  dot.fill( dot_colour )
  dot.center((fret_l*x-1.5*fret_l+fret_l),(1.0*string_sep));
  var dot = draw.circle(dot_r);
  dot.fill( dot_colour )
  dot.center((fret_l*x-1.5*fret_l+fret_l),(5.0*string_sep));
}


function romanize(num) {
  var lookup = {M:1000,CM:900,D:500,CD:400,C:100,XC:90,L:50,XL:40,X:10,IX:9,V:5,IV:4,I:1},roman = '',i;
  for ( i in lookup ) {
    while ( num >= lookup[i] ) {
      roman += i;
      num -= lookup[i];
    }
  }
  return roman;
}


function draw_neck() {
//SVG.on(document, 'DOMContentLoaded', function() {

  // ROTATE TEXTS:
  // false: no rotation
  // true:  rotation
  const ROTATE = false

  const highlightcolor = "Lime";

  //  CONSTANTS
  const INTERVALS = [ '1', 'p2', 'S2', 'p3', 'S3', '4',
    '-5', '5', 'p6', 'S6', 'p7', 'S7'];

  // PARAMETERS
  const my_l = 1250;
  const my_w = 150;
  const dot_r = 25;
  const font_size = 12;
  const no_frets = 24;
  const fret_l = my_l/(no_frets + 1.0);
  const string_sep = my_w/6;
  const bubble_d = 0.80*string_sep;

  // COLOURS
  // https://www.w3schools.com/tags/ref_colornames.asp
  const fretboard_colour = 'BurlyWood';
  const fret_colour = 'DimGray';
  const string_colour = 'DarkGoldenRod';
  const dot_colour = 'Black'
  const bubble_opacity = 0.75

  // START DRAWING
  //var drawing = SVG('neck');
  var drawing = SVG().addTo('body').size((my_l), (my_w+string_sep))
  drawing.addClass('svgTop')

  //background = draw_background( 'neck' );

  // NECK
  var board = drawing.rect(my_l, my_w);
  board.fill(fretboard_colour);
  board.move( fret_l, 0 );

  // FRETS + FRET NUMBERS
  for ( var x=0; x<=no_frets; x++){
    var fret = drawing.line( (x*fret_l + fret_l), 0, (x*fret_l + fret_l), my_w );
    fret.stroke({ width:4 });
    // NUT IS BIGGER
    if (x == 0) {
      fret.stroke({ width:8 });
    }
    fret.stroke(fret_colour);
    // FRET NUMBERS
    if (x != 0) {
      var fret_number = drawing.text( romanize(x) );
      fret_number.addClass('text')
      fret_number.font( 'size', font_size);
      if ( [12, 24].includes(x) ) {
        fret_number.font('size', font_size+5);
        fret_number.font('weight', 'bold');
      }
      if ( Boolean( ROTATE ) ) {
        fret_number.rotate(270, fret_number.cx(), fret_number.cy() )
      }
      fret_number.center( x*fret_l + 0.5*fret_l, 6.5*string_sep );
    }
  }

  // STRINGS
  for (y=0; y<6; y++){
    var y_value = 0.5*string_sep + y*string_sep;
    var string = drawing.line( fret_l, y_value, my_l, y_value );
    string.stroke({ width:(0.6*y+2) });
    string.stroke(string_colour);
  }

  // DECORATIONS
  const dot_x = [3,5,7,9,15,17,19,21];
  for (x of dot_x) {
    var dot = drawing.circle(dot_r);
    dot.fill( dot_colour )
    dot.center( (fret_l*x - 0.5*fret_l + fret_l), (0.5*my_w) );
  }
  x = 12;
  var dot = drawing.circle(dot_r);
  dot.fill( dot_colour )
  dot.center((fret_l*x-0.5*fret_l+fret_l),(1.0*string_sep));
  var dot = drawing.circle(dot_r);
  dot.fill( dot_colour )
  dot.center((fret_l*x-0.5*fret_l+fret_l),(5.0*string_sep));
  x = 24;
  var dot = drawing.circle(dot_r);
  dot.fill( dot_colour )
  dot.center((fret_l*x-0.5*fret_l+fret_l),(1.0*string_sep));
  var dot = drawing.circle(dot_r);
  dot.fill( dot_colour )
  dot.center((fret_l*x-0.5*fret_l+fret_l),(5.0*string_sep));


  // CREATE GROUP FOR NOTES
  //var notegroup = drawing.nested()
  var notegroup = drawing.nested().size( 5*my_l, my_w )
  notegroup.move( -24*fret_l, 0 )


  // FUNCTION TO PLACE INTERVAL MARKINGS
  function  placeIntervals( string_number, fret_for_base ){
    for (i=0; i<INTERVALS.length; i++){
      center_x = (fret_for_base+0.5)*fret_l + i*fret_l;
      center_y = (string_number-0.5)*string_sep; 

      var bubble = drawing.circle( bubble_d );
      bubble.addClass('slider')
      bubble.fill("white");
      bubble.opacity( bubble_opacity );
      bubble.center(center_x, center_y);

      var interval = drawing.text( INTERVALS[i] );
      interval.addClass('slider')
      interval.addClass('text')
      if ( Boolean( ROTATE ) ) {
        interval.rotate(270, center_x, center_y)
      }
      interval.font( 'size', font_size);
      while ( interval.length() > (0.8*string_sep) ) {
        my_size = interval.font('size')
        my_size = my_size - 0.1
        interval.font( 'size', my_size )
      }
      interval.center(center_x, center_y);

      // BUBBLE COLOURING
      if (INTERVALS[i] == '1') {
        bubble.addClass('1')
        bubble.fill(highlightcolor);
        bubble.stroke({ color:"black", width: 3});
      }
      if (INTERVALS[i] == 'p2') { bubble.addClass('p2') }
      if (INTERVALS[i] == 'S2') { bubble.addClass('S2') }
      if (INTERVALS[i] == 'p3') { bubble.addClass('p3') }
      if (INTERVALS[i] == 'S3') { bubble.addClass('S3'); bubble.fill(highlightcolor); }
      if (INTERVALS[i] == '4')  { bubble.addClass('4') }
      if (INTERVALS[i] == '-5') { bubble.addClass('-5') }
      if (INTERVALS[i] == '5')  { bubble.addClass('5'); bubble.fill(highlightcolor); }
      if (INTERVALS[i] == 'p6') { bubble.addClass('p6') }
      if (INTERVALS[i] == 'S6') { bubble.addClass('S6') }
      if (INTERVALS[i] == 'p7') { bubble.addClass('p7') }
      if (INTERVALS[i] == 'S7') { bubble.addClass('S7') }

      notegroup.add(bubble);
      notegroup.add(interval);
    }
  }

  for (x of [-24,-12,0,12,24,36,48,60]) {
    placeIntervals(1,8+x);
    placeIntervals(2,1+x);
    placeIntervals(3,5+x);
    placeIntervals(4,10+x);
    placeIntervals(5,3+x);
    placeIntervals(6,8+x);
  }

  // NOTEGROUP TO THE FRONT AND SET THE INITIAL LOCATION
  notegroup.front();
  var notegroup_x_start = notegroup.x()
  var notegroup_x = notegroup_x_start

  var move_right = function() {
    if ( notegroup_x < (notegroup_x_start + 24*fret_l) ){
      notegroup_x = notegroup_x + fret_l;
    }
    notegroup.animate(250, '<>', 'now').x( notegroup_x );
  }
  var move_left = function() {
    if ( notegroup_x > (notegroup_x_start - 24*fret_l) ){
      notegroup_x = notegroup_x - fret_l;
    }
    notegroup.animate(250, '<>', 'now').x( notegroup_x );
  }


  // CLICK SENSE AREAS
  var sensor_left = drawing.rect(0.5*my_l, my_w);
  sensor_left.attr({ opacity: 0.0 });

  var sensor_right = drawing.rect(0.5*my_l, my_w);
  sensor_right.move(0.5*my_l, 0);
  sensor_right.attr({ opacity: 0.0 });


  // CLICK EVENT -> CALL ANIMATION FUNCTION
  //sensor_left.on('click', move_left);
  //sensor_right.on('click', move_right);

//}) // END SVG.on(
}


function draw_background( drawing ) {

  var my_l   = 1500;
  var my_w   =  200;
  var no_frets = 24;
  var fret_l =  my_l/(no_frets + 1.0);
  var string_sep = my_w/6;
  var fretboard_colour = 'BurlyWood';

  //  START DRAWING
  //var draw = SVG('tausta');
  //draw.size(my_l, my_w);
  //var draw = drawing.nested();
  var draw = SVG.get( drawing )

  //  NECK
  var board = draw.rect(my_l, my_w);
  board.fill( fretboard_colour );
  board.move( fret_l, 0 );

  return board;
}

//draw_neck()
//testi()
