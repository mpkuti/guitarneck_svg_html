#!/bin/bash

# OPTIMIZE ALL .svg FILES IN THIS DIRECTORY
for filename in ./*.svg; do

    # DISCARD THE .svg FROM THE FULENAME
    filename_body="${filename%.*}"

    # CREATE THE OUTPUT FILENAME
    output_filename=$filename_body".svg.optimized"

    # EXECUTE THE OPTIMIZATION USING scour
    scour -i $filename -o $output_filename --enable-viewboxing --enable-id-stripping \
	      --enable-comment-stripping --shorten-ids --indent=none

    # LOG
    echo $filename" -> "$output_filename

done
